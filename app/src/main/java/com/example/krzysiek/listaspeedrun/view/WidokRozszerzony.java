package com.example.krzysiek.listaspeedrun.view;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.krzysiek.listaspeedrun.R;

import java.util.ArrayList;

public class WidokRozszerzony extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_widokrozszerzony);

        getIncomingIntent();

    }

    private void getIncomingIntent(){
        if (getIntent().hasExtra("obrazek") && getIntent().hasExtra("tekst") && getIntent().hasExtra("position") && getIntent().hasExtra("obrazki") && getIntent().hasExtra("tytul")){
            Integer position = getIntent().getExtras().getInt("position", 0);
            ArrayList<Integer> obrazki = getIntent().getIntegerArrayListExtra("obrazki");
            String obrazek = getIntent().getStringExtra("obrazek");
            String tekst = getIntent().getStringExtra("tekst");
            String tytul = getIntent().getStringExtra("tytul");
            setImage(obrazki, obrazek, tekst, position, tytul);
        }
    }

    private void setImage(ArrayList<Integer> obrazki, String obrazek, String tekst, Integer position, String tytul){
        TextView nazwa = findViewById(R.id.szczegolowyTxt);
        nazwa.setText(tekst);
        TextView nazwaTytul = findViewById(R.id.szczegolowyTytul);
        nazwaTytul.setText(tytul);
        ImageView image = findViewById(R.id.szczegolowyImage);
        image.setImageResource(obrazki.get(position));
    }
}
