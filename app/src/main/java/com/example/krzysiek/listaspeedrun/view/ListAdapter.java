package com.example.krzysiek.listaspeedrun.view;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.krzysiek.listaspeedrun.R;
import com.example.krzysiek.listaspeedrun.data.DataSource;

import java.util.ArrayList;


public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    private ArrayList<String> mText = new ArrayList<>();
    private ArrayList<Integer> mImages = new ArrayList<>();
    private ArrayList<String> mSzczegolText = new ArrayList<>();
    private Context mContext;

    public ListAdapter(Context mContext, ArrayList<String> mText,  ArrayList<Integer> mImages , ArrayList<String> mSzczegolText) {
        this.mText = mText;
        this.mImages = mImages;
        this.mContext = mContext;
        this.mSzczegolText = mSzczegolText;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        //tu moze nie dzialac
        holder.image.setImageResource(mImages.get(position));
        holder.text.setText(mText.get(position));

        holder.layout_list_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, mText.get(position), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(mContext, WidokRozszerzony.class);
                intent.putIntegerArrayListExtra("obrazki", mImages);
                intent.putExtra("position", position);
                intent.putExtra("obrazek", mImages.get(position));
                intent.putExtra("tytul", mText.get(position));
                intent.putExtra("tekst", mSzczegolText.get(position));
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mText.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView image;
        TextView text;
        RelativeLayout layout_list_item;
        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.item_image);
            text = itemView.findViewById(R.id.item_text);
            layout_list_item = itemView.findViewById(R.id.layout_list_item);
        }
    }
}
