package com.example.krzysiek.listaspeedrun.view;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.krzysiek.listaspeedrun.R;

import java.util.ArrayList;

public class ListaSpeedrun extends AppCompatActivity  {

    private ArrayList<String> mText = new ArrayList<>();
    private ArrayList<String> mSzczegolText = new ArrayList<>();
    private ArrayList<Integer> mImage = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_speedrun);

        data();
        initRecyclerView();
    }

    private void data(){
        mText.add("Gta vice city");
        mSzczegolText.add("1st Marushko 51m 11s 23-11-2018");
        mImage.add(R.drawable.gtavcicon);

        mText.add("Celeste");
        mSzczegolText.add("1st TGH 28m49s 14-11-2018");
        mImage.add(R.drawable.celaste);

        mText.add("Nfs Most Wanted");
        mSzczegolText.add("1st Kuru 3h 45m 38s 17-04-2018");
        mImage.add(R.drawable.nfs);

        mText.add("Zelda Botw");
        mSzczegolText.add("1st Wolhaiksong 31m 06s 17-11-2018");
        mImage.add(R.drawable.zelda_breathofthewild);

        mText.add("A hat of time");
        mSzczegolText.add("1st doesthisusername 43m 12s 610ms 15-11-2018");
        mImage.add(R.drawable.ahatoftime);

        mText.add("Bloodborne");
        mSzczegolText.add("1st InSilico_29m 08s 21-08-2018");
        mImage.add(R.drawable.bloodborne);

        mText.add("Cluster Truck");
        mSzczegolText.add("1st snakeathon 18m 24s 740ms 11-10-2018");
        mImage.add(R.drawable.cluster_truck);

        mText.add("Cuphead");
        mSzczegolText.add("1st SBDWolf 23m 41s 23-11-2018");
        mImage.add(R.drawable.cuphead);

        mText.add("Diablo");
        mSzczegolText.add("1st Funkmastermp 12m 55s 03-01-2015");
        mImage.add(R.drawable.diablo);

        mText.add("Hollow Knight");
        mSzczegolText.add("1st Skurry 34m 19s 30-10-2018");
        mImage.add(R.drawable.hollow_knight);

        mText.add("Messenger");
        mSzczegolText.add("1st KuningasEST 32m 10s 22-11-2018");
        mImage.add(R.drawable.messenger);

        mText.add("Nier");
        mSzczegolText.add("1st Kanaris 1h 24m 35s 240ms 16-09-2018");
        mImage.add(R.drawable.nier);

        mText.add("Ori the blind forest");
        mSzczegolText.add("1st Lucidus 27m 59s 23-11-2018");
        mImage.add(R.drawable.ori);

        mText.add("Portal");
        mSzczegolText.add("1st CantEven 7m 16s 740ms 13-11-2018");
        mImage.add(R.drawable.portal);

        mText.add("Splatoon2");
        mSzczegolText.add("1st hashedrice 1h 32m 31s 07-03-2017");
        mImage.add(R.drawable.splatoon2);

        mText.add("Super Mario Bros");
        mSzczegolText.add("1st somewes 4m 55s 796ms 19-10-2018");
        mImage.add(R.drawable.super_mario_bros);

        mText.add("Super Meat Boy");
        mSzczegolText.add("1st vorpal 17m 35s 12-11-2018");
        mImage.add(R.drawable.supermeatboy);

        mText.add("Tetris");
        mSzczegolText.add("1st JdMfX_ 3m 09s 06-06-2018");
        mImage.add(R.drawable.tetris);

        mText.add("Undertale");
        mSzczegolText.add("1st SnowieY 56m 19s 04-09-2018");
        mImage.add(R.drawable.undertale);

        mText.add("VVVVVV");
        mSzczegolText.add("1st tzann 12m 03s 13-09-2018");
        mImage.add(R.drawable.vvvvvv);

    }

    private void initRecyclerView(){
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        ListAdapter adapter = new ListAdapter(this, mText, mImage, mSzczegolText);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}
