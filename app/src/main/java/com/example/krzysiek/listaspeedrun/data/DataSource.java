package com.example.krzysiek.listaspeedrun.data;

import com.example.krzysiek.listaspeedrun.R;

import java.util.ArrayList;
import java.util.List;

public class DataSource {

    public static String[] title = new String[] {
            "Gta vice city",
            "Celaste",
            "Nfs most wanted",
            "A hat of time",
            "Bloodborne",
            "Cluster_truck",
            "Cuphead",
            "Diablo",
            "Hollow knight",
            "Messenger",
            "Nier",
            "Ori",
            "Portal",
            "Splatoon2",
            "Super mario bros",
            "Super meat boy",
            "Tetris",
            "Undertale",
            "Vvvvvv",
            "Zelda Breath of the wild",
    };

    public static int[] picturePath = new int[]{
            R.drawable.gtavcicon,
            R.drawable.celaste,
            R.drawable.nfs,
            R.drawable.ahatoftime,
            R.drawable.bloodborne,
            R.drawable.cluster_truck,
            R.drawable.cuphead,
            R.drawable.diablo,
            R.drawable.hollow_knight,
            R.drawable.messenger,
            R.drawable.nier,
            R.drawable.ori,
            R.drawable.portal,
            R.drawable.splatoon2,
            R.drawable.super_mario_bros,
            R.drawable.supermeatboy,
            R.drawable.tetris,
            R.drawable.undertale,
            R.drawable.vvvvvv,
            R.drawable.zelda_breathofthewild,


    };
}
